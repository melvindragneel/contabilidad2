Create database Cuentas;
use Cuentas;
Create table Cuentas(ID numeric primary key,Nombre Varchar(100),Naturaleza Varchar(100));
Create table Movimientos(ID numeric primary key,Descripcion Varchar(100),Monto numeric,Fecha date,ID_cuenta numeric,Debehaber Varchar(100), foreign key(ID_cuenta) references Cuentas(ID));
